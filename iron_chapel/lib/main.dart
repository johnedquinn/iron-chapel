import 'package:flutter/material.dart';
import 'package:iron_chapel/pages/nutrition_page.dart';
import 'package:iron_chapel/pages/sign_in.dart';
import 'pages/routines.dart';
import 'pages/routine.dart';
import 'pages/profile.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        brightness: Brightness.light,
        primarySwatch: Colors.blue,
      ),
      routes: {
        '/': (BuildContext context) => SignInPage(),
        '/base': (BuildContext context) => BasePage(),
        //'/exercise': (BuildContext context) => ExercisePage(),
      },
      onGenerateRoute: (RouteSettings settings) {
        List<String> stringList = settings.name.split('/');
        if (stringList[1] == 'routine') {
          return MaterialPageRoute(
            builder: (BuildContext context) => RoutinePage(stringList[2]),
          );
        } else {
          return MaterialPageRoute(
            builder: (BuildContext context) => BasePage(),);
          }//else if (stringList[1] == 'exercise') {
          //return MaterialPageRoute(
           // builder: (BuildContext context) => ExercisePage(stringList[2]),
          //);
        //}
      },
    );
  }
}

class BasePage extends StatelessWidget {
  final List<Widget> pages = [
    RoutinesPage(),
    NutritionPage(),
    ProfilePage(),
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      initialIndex: 0,
      child: Scaffold(
        body: TabBarView(
          children: pages,
        ),
        bottomNavigationBar: Container(
          // decoration: BoxDecoration(
          //   boxShadow: [
          //     BoxShadow(
          //       offset: Offset(10, 10)
          //     )
          //   ]
          // ),
          margin: EdgeInsets.only(bottom: 10),
          child: new TabBar(
            tabs: [
              Tab(
                icon: Icon(Icons.fitness_center),
              ),
              Tab(
                icon: Icon(Icons.local_dining)
              ),
              Tab(
                icon: Icon(Icons.portrait),
              ),
            ],
            unselectedLabelColor: Colors.blueGrey,
            labelColor: Colors.black,
            indicatorColor: Colors.transparent,
          ),
        ),
      ),
    );
  }
}
