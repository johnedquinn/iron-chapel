import 'package:flutter/material.dart';
import 'package:iron_chapel/classes/history_info.dart';
import 'package:iron_chapel/widgets/history_section.dart';
import 'package:iron_chapel/widgets/progress_section.dart';


class HistoryController extends StatefulWidget {
  HistoryInfo _historyInfo;
  int attemptedSets;
  int attemptedReps;

  HistoryController(this._historyInfo, {@required this.attemptedSets, @required this.attemptedReps});

  @override
  State<StatefulWidget> createState() {
    return _HistoryControllerState();
  }
}

class _HistoryControllerState extends State<HistoryController> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _buildHistoryController();
  }

  Widget _buildHistoryController() {
    return Container(
      alignment: Alignment.centerLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _getProgressSection(),
          _getHistorySection()
        ],
      ),
    );
  }

  Widget _getProgressSection() {
    return ProgressSection(historyInfo: widget._historyInfo);
  }

  Widget _getHistorySection() {
    return HistorySection(widget._historyInfo);
  }

}
