/*
 * File: exercise_strength_index.dart
 * Author: John Ed Quinn
 * Description: Holds the information necessary for creating a chart
*/

/* Class: ExerciseStrengthIndex */
class ExerciseStrengthIndex {

  /// Variables
  int strength;   // Creates the determined 'strength index' for an exercise
  DateTime date;  // The date of when the exercise was performed

  /// Function: ExerciseStrengthIndex
  /// Description: Constructor
  ExerciseStrengthIndex(this.strength, this.date);

}