
import 'package:flutter/material.dart';

class Exercise {
    String _title;
    String _description;
    Exercise(this._title, [this._description]);
    String getTitle() { return this._title; }
    String getDescription() { return _description; }
}

class SuperSet {
  List <Exercise> _list;
  SuperSet(this._list);
  List <Exercise> getList() { return this._list; }
}

class Routine {

  /* Variables */
  String _title;
  String _description;
  Color _color;
  List <dynamic> _exercises;

  /* Constructor */
  Routine(this._title, [this._description, this._color, this._exercises]);

  /* Get Methods */
  String getTitle() { return _title; }
  String getDescription() {return this._description; }
  Color getColor() { return this._color; }
  List <dynamic> getList() {return this._exercises; }
  
  /* Set Methods */
  void editTitle(String title) { this._title = title; }
  void addExercise(Exercise target) { this._exercises.add(target); }
  void removeExercise(Exercise target) { this._exercises.remove(target); }
  
  /* Get Number of Exercises */
  int getNumExercises() {
    int num = 0;
    for (int i = 0; i < _exercises.length; i++) {
      if (_exercises[i] is Exercise) { num++; }
      else if (_exercises[i] is SuperSet) {
        List <Exercise> ss = _exercises[i].getList();
        for (int j = 0; j < ss.length; j++) {
          num++;
        }
      }
    }
    return num;
  }
}

class MuscleGroup {
  String _muscleGroup;
  List <Exercise> _exercises;
  MuscleGroup(this._muscleGroup, [this._exercises]);
  String getName() { return _muscleGroup; }
  List <Exercise> getList() { return this._exercises; }
  void changeName(String group) { this._muscleGroup = group; }
  void addExercise(Exercise target) { _exercises.add(target); }
  void deleteExercise(Exercise target) { _exercises.remove(target); }
}

class MuscleGroups {
  List <MuscleGroup> groups = [MuscleGroup("Chest"), MuscleGroup("Back"), MuscleGroup("Legs")];
}