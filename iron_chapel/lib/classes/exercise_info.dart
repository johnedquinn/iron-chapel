/*
 * File: exercise_info.dart
 * Author: John Ed Quinn
 * Description: Variable `exerciseInfo` is a DocumentSnapshot containing the informaion ...
 * ... as follows:
 * --> {
 * --> }
 * ... The `exerciseInfo` is located at /exercises/exerciseNumber
*/

/* Imports */
import 'package:cloud_firestore/cloud_firestore.dart';

/* Class: ExerciseInfo */
class ExerciseInfo {

  /// Variables
  var exerciseInfo;  // Stores the DocumentSnapshot

  /// Function: ExerciseInfo
  /// Description: Constructor
  ExerciseInfo({this.exerciseInfo = null});

  /// Function: getData
  /// Description: Returns the data stored in exerciseInfo.
  dynamic getData() {
    return this.exerciseInfo;
  }

  /// Function: getExerciseDocument
  /// Description: Asynchronous function to grab the ExerciseInfo DocumentSnapshot ...
  /// ... from the '/exercises/exerciseNum' file.
  Future <void> getExerciseDocument(String doc) async {
    if (this.exerciseInfo == null) {
      DocumentSnapshot ds =
          await Firestore.instance.collection('/exercises').document(doc).get();
      this.exerciseInfo = ds;
    }
  }

}
