/*
 * File: history_info.dart
 * Author: John Ed Quinn
 * Description: Variable `_historyInfo` is a DocumentSnapshot containing the informaion ...
 * ... as follows:
 * --> {'completed': 
 * --> [ {'attempted': string,
 * --> 'date': timestamp,
 * --> 'reps': string,
 * --> 'weights': string} ]
 * --> }
 * ... The `historyInfo` is located at /users/userNumber/exercises/exerciseNumber
*/

/* Imports */
import 'package:cloud_firestore/cloud_firestore.dart';

/* Class: HistoryInfo */
class HistoryInfo {

  /// Variables
  var _historyInfo;  // Stores the DocumentSnapshot

  /// Function: HistoryInfo
  /// Description: Constructor
  HistoryInfo() {
    this._historyInfo = null;
  }

  /// Function: getData
  /// Description: Returns the data stored in _historyInfo.
  dynamic getData() {
    return _historyInfo;
  }

  /// Function: getHistoryDocument
  /// Description: Asynchronous function to grab the HistoryInfo DocumentSnapshot ...
  /// ... from the '/users/userNum/exercises/exerciseNum' file.
  Future<void> getHistoryDocument(String doc) async {
    if (this._historyInfo == null) {
      DocumentSnapshot ds = await Firestore.instance
          .collection('/users/U3oI4eEr72P6ZX89O2XT2ls8sLx2/exercises')
          .document(doc)
          .get();
      this._historyInfo = ds;
    }
  }

}
