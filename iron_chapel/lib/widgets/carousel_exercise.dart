import 'package:flutter/material.dart';
import 'package:iron_chapel/classes/history_info.dart';
import 'package:iron_chapel/widgets/active_card.dart';
import 'history_card.dart';

class CarouselExercise extends StatelessWidget {
  /// Variables
  var exercise; // Stores the actual exercise name, description, etc.
  var exerciseInfo; // Stores the path to the exercise, the sets, and reps.
  HistoryInfo historyInfo;

  /// Constructor
  CarouselExercise(this.exercise, this.exerciseInfo, {@required this.historyInfo}) {
    print('\nCAROUSEL EXERCISE');
    print('Exercise = ' + exercise.toString());
    print('ExerciseInfo = ' + exerciseInfo.toString());
  }

  /// Build Method
  @override
  Widget build(BuildContext context) {
    return buildExerciseCard();
  }

  /// Builds the container to hold the content
  Widget buildExerciseCard() {
    return Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.0),
            color: Colors.grey,
            border: Border.all(width: 1.0, color: Colors.black)),
        child: buildExerciseContent());
  }

  /// Organizes the data inside the card
  Widget buildExerciseContent() {
    int index = this.historyInfo.getData()['completed'].length - 1;
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            getExerciseName(),
            getExerciseInfoIcon(),
          ],
        ),
        Row(children: <Widget>[getSetsAndReps()]),
        Row(
          children: <Widget>[getLastWeekTitle(), getSeeMoreText()],
        ),
        HistoryCardWidget(historyInfo: this.historyInfo, index: index),
        Row(
          children: <Widget>[getTodayTitle()],
        ),
        ActiveCardWidget(exerciseInfo: this.exerciseInfo),
        /*Container(
          height: 100,
          child: ListView.separated(
            padding: EdgeInsets.all(5.0),
            scrollDirection: Axis.horizontal,
            itemCount: this.exerciseInfo['sets'],
            itemBuilder: (BuildContext context, int index) {
              return WeightsRepsWidget(false);
            },
            separatorBuilder: (BuildContext context, int index) {
              return Container(width: 15);
            },
          ),
        ),*/
      ],
    );
  }

  /// Returns the name of the exercise
  Widget getExerciseName() {
    return Container(
      child: Text(
        this.exercise.getData()['title'],
        style: TextStyle(
            color: Colors.white, fontSize: 35.0, fontWeight: FontWeight.bold),
      ),
      padding: EdgeInsets.fromLTRB(13.0, 13.0, 13.0, 0.0),
    );
  }

  /// Returns the info icon
  Widget getExerciseInfoIcon() {
    return Icon(
      Icons.info,
      color: Colors.white,
      size: 15.0,
    );
  }

  /// Returns the sets and reps
  Widget getSetsAndReps() {
    /* Get the string for SETS x REPS */
    String setsAndReps = this.exerciseInfo['sets'].toString() +
        'x' +
        this.exerciseInfo['reps'].toString();
    /* Declare the TextStyle for the SETS x REPS */
    TextStyle _style = TextStyle(
        fontSize: 20.0,
        fontWeight: FontWeight.bold,
        color: Colors.white,
        letterSpacing: 1.5);
    /* Declare the Padding for the SETS x REPS */
    EdgeInsetsGeometry _padding = EdgeInsets.fromLTRB(14.0, 0.0, 14.0, 0.0);
    /* Return the Text contains the SETS x REPS */
    return Container(
        child: Text(setsAndReps, style: _style, textAlign: TextAlign.left),
        padding: _padding);
  }

  Widget getLastWeekTitle() {
    TextStyle _style = TextStyle(color: Colors.white, fontSize: 16);
    EdgeInsetsGeometry _padding = EdgeInsets.fromLTRB(14.0, 10.0, 14.0, 0.0);
    return Container(
      child: Text('Most Recent', style: _style),
      padding: _padding,
    );
  }

  Widget getSeeMoreText() {
    TextStyle _style = TextStyle(color: Colors.white);
    EdgeInsetsGeometry _padding = EdgeInsets.fromLTRB(14.0, 10.0, 14.0, 0.0);
    return Container(
      alignment: Alignment.bottomRight,
      child: Text('See More', style: _style),
      padding: _padding,
    );
  }

  Widget getTodayTitle() {
    TextStyle _style = TextStyle(color: Colors.white, fontSize: 16);
    EdgeInsetsGeometry _padding = EdgeInsets.fromLTRB(14.0, 10.0, 14.0, 0.0);
    return Container(
      child: Text('Today', style: _style),
      padding: _padding,
    );
  }
}
