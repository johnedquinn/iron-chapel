import 'package:flutter/material.dart';

class WeightsRepsWidget extends StatefulWidget {
  bool isLastWeek;

  WeightsRepsWidget(this.isLastWeek);

  @override
  State<StatefulWidget> createState() {
    return _WeightRepsWidgetState();
  }
}

class _WeightRepsWidgetState extends State<WeightsRepsWidget> {
  @override
  Widget build(BuildContext context) {
    return getContainer();
  }

  Widget getContainer() {
    EdgeInsetsGeometry _padding = EdgeInsets.fromLTRB(13, 0, 13, 0);
    Widget top =
        (widget.isLastWeek) ? getLastWeekField() : getTextField('Lbs.');
    Widget bottom =
        (widget.isLastWeek) ? getLastWeekField() : getTextField('Reps');
    return Container(
      decoration: BoxDecoration(
          border: Border(),
          borderRadius: BorderRadius.circular(5),
          color: Colors.white),
      height: 80,
      width: 80,
      padding: _padding,
      child: Column(
        children: <Widget>[top, bottom],
      ),
    );
  }

  Widget getTextField(String text) {
    return TextFormField(
      decoration: InputDecoration(border: InputBorder.none, hintText: text),
      keyboardType: TextInputType.number,
    );
  }

  Widget getLastWeekField() {
    return Text('5');
  }
}
