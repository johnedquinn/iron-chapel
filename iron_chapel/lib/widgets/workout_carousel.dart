import 'package:flutter/material.dart';
import 'package:iron_chapel/classes/history_info.dart';
import 'package:iron_chapel/widgets/carousel_exercise.dart';

class WorkoutCarousel extends StatefulWidget {
  /// Private Variables
  var _workout;
  List<dynamic> exercises;
  List <HistoryInfo> histories;

  /// Constructor
  WorkoutCarousel(this._workout, this.exercises, {@required this.histories}) {
    print('\nWORKOUT CAROUSEL');
    print('Exercises: ' + this.exercises.toString());
    print('Workout: ' + this._workout.toString());
  }

  /// Create State
  @override
  State<StatefulWidget> createState() {
    return _WorkoutCarouselState();
  }
}

class _WorkoutCarouselState extends State<WorkoutCarousel> {
  /// Variables
  PageController controller;
  int currentpage = 0;
  //List<dynamic> exercises = List<dynamic>();

  /// Init State
  @override
  initState() {
    super.initState();
    controller = new PageController(
      initialPage: currentpage,
      keepPage: true,
      viewportFraction: 0.95,
    );
  }

  /// Build method
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 500,
      child: new PageView.builder(
          itemCount: widget._workout['exercises'].length,
          onPageChanged: (value) {
            setState(() {
              currentpage = value;
            });
          },
          controller: controller,
          itemBuilder: (context, index) => builder(index)),
    );
  }

  /// Dispose method
  @override
  dispose() {
    controller.dispose();
    super.dispose();
  }

  /// Builder method
  Widget builder(int index) {
    return new AnimatedBuilder(
        animation: controller,
        builder: (context, child) {
          double value = 1.0;
          if (controller.position.haveDimensions) {
            value = controller.page - index;
            value = (1 - (value.abs() * .3)).clamp(0.0, 1.0);
          }

          return new Center(
            child: new SizedBox(
              height: Curves.easeOut.transform(value) * 500,
              child: child,
            ),
          );
        },
        child: getExerciseCard(index));
  }

  Widget getExerciseCard(int index) {
    print(index);
    print(widget.exercises[index]);
    return CarouselExercise(
        widget.exercises[index], widget._workout['exercises'][index], historyInfo: widget.histories[index],);
  }

}
