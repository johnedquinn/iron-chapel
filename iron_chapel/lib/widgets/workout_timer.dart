import 'package:flutter/material.dart';
import 'dart:async';

class WorkoutTimer extends StatefulWidget {
  final Stopwatch stopwatch = Stopwatch();

  void stop() {
    _watch.stop();
  }

  Stopwatch _watch = Stopwatch();
  @override
  State<StatefulWidget> createState() {
    return _WorkoutTimerState();
  }
}

class _WorkoutTimerState extends State<WorkoutTimer> {
  Timer timer;

  _WorkoutTimerState() {
    timer = new Timer.periodic(new Duration(seconds: 1), callback);
  }

  @override
  void initState() {
    super.initState();
    widget.stopwatch.start();
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  void callback(Timer timer) {
    if (widget.stopwatch.isRunning) {
      setState(() {});
    }
  }

  String toText(Stopwatch target) {
    int seconds = (target.elapsedMilliseconds / 1000).truncate();
    int minutes = (seconds / 60).truncate();
    int hours = (minutes / 60).truncate();
    String hoursStr = (hours % 60).toString().padLeft(2, '0');
    String minutesStr = (minutes % 60).toString().padLeft(2, '0');
    String secondsStr = (seconds % 60).toString().padLeft(2, '0');
    return (hours > 0)
        ? "$hoursStr:$minutesStr:$secondsStr"
        : "$minutesStr:$secondsStr";
  }

  @override
  Widget build(BuildContext context) {
    return Text(
      toText(widget.stopwatch),
      style: TextStyle(
          fontSize: 60.0, color: Colors.white, fontWeight: FontWeight.bold),
    );
  }
}
