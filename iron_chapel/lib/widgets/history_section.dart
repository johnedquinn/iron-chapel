import 'package:flutter/material.dart';
import 'package:iron_chapel/classes/history_info.dart';
import 'package:iron_chapel/widgets/history_card.dart';

/*
 * HistorySection
 * 
 * 
 */

class HistorySection extends StatelessWidget {

  HistoryInfo _historyInfo;

  HistorySection(this._historyInfo) {
    print('HISTORY SECTION');
    print('History info: ' + _historyInfo.toString());
  }

  @override
  Widget build(BuildContext context) {
    return _buildHistorySection(context);
  }

  Widget _buildHistorySection(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[_getRecentHistoryTitle(), _getHistoryList(context)],
      ),
    );
  }

  Widget _getRecentHistoryTitle() {
    EdgeInsetsGeometry _padding = EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 10.0);
    TextStyle _style = TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold);
    return Container(child: Text('History', style: _style), padding: _padding);
  }

  Widget _getHistoryList(BuildContext context) {
    if (this._historyInfo != null) {
      return Column(children: _buildColumn(context));
    } else {
      return Text('No history with this exercise');
    }
  }

  List<Widget> _buildColumn(BuildContext context) {
    int maxLength = 3;
    bool isLong = this._historyInfo.getData()['completed'].length > maxLength;
    int length = this._historyInfo.getData()['completed'].length;
    int size = (isLong) ? maxLength : this._historyInfo.getData()['completed'].length;
    List<Widget> _columnItems = List<Widget>();
    for (int i = length - 1; i > length - size - 1; i--) {
      _columnItems.add(_createHistoryCard(context, i));
    }
    if (isLong) _columnItems.add(_buildViewMore());
    return _columnItems;
  }

  Widget _createHistoryCard(BuildContext context, int index) {
    return HistoryCardWidget(historyInfo: this._historyInfo, index: index);
  }
/*
  Widget _createHistoryCard(BuildContext context, int index) {
    return Card(
        color: Color(4293848814),
        margin: EdgeInsets.fromLTRB(15.0, 7.0, 15.0, 7.0),
        elevation: 2,
        child: Container(
          height: 110,
          child: Row(
            children: <Widget>[
              _buildHistoryCardInfoSection(index),
              _buildHistoryCardRepsList(index)
            ],
          ),
        ));
  }

  Widget _buildHistoryCardInfoSection(int cardIndex) {
    DateTime _date = this._historyInfo.getData()['completed'][cardIndex]['date'].toDate();
    String _attemptedSetsAndReps = this._historyInfo.getData()['completed'][cardIndex]['attempted'];
    return Container(
      width: 100,
      padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
      child: Center(
          child: HistoryCardDate(_date, attemptedSetsAndReps: _attemptedSetsAndReps)),
    );
  }

  Widget _buildHistoryCardRepsList(int cardIndex) {
    return Expanded(
        child: Container(
      padding: EdgeInsets.all(6),
      child: ListView.separated(
        scrollDirection: Axis.horizontal,
        itemCount: 4,
        padding: new EdgeInsets.symmetric(vertical: 2.0),
        itemBuilder: (BuildContext context, int setIndex) {
          return _buildWeightsAndRepsSubCard(context, cardIndex, setIndex);
        },
        separatorBuilder: (BuildContext context, int index) {
          return Container(
            width: 5,
            padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
          );
        },
      ),
    ));
  }

  Widget _buildWeightsAndRepsSubCard(
      BuildContext context, int cardIndex, int setIndex) {
    return Container(
      height: 10,
      width: 100,
      //color: Colors.white,
      child: Column(
        children: <Widget>[
          _buildSubCardSetTitle(context, setIndex),
          _buildSubCard(context, cardIndex, setIndex)
        ],
      ),
    );
  }

  Widget _buildSubCardSetTitle(BuildContext context, int index) {
    return Text('Set ' + (index + 1).toString());
  }

  Widget _buildSubCard(BuildContext context, int cardIndex, int setIndex) {
    List<String> _weights =
        this._historyInfo.getData()['completed'][cardIndex]['weights'].split('/');
    List<String> _reps =
        this._historyInfo.getData()['completed'][cardIndex]['reps'].split('/');
    return Expanded(
      child: Card(
        color: Colors.white,
        child: Container(
          constraints: BoxConstraints.expand(),
          //child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _buildSubCardWeightText(context, _weights, setIndex),
              Divider(),
              _buildSubCardRepsText(context, _reps, setIndex)
            ],
          ),//)
        ),
      ),
    );
  }

  Widget _buildSubCardWeightText(
      BuildContext context, List<String> weights, int setIndex) {
    String _text = weights[setIndex] + ' lbs.';
        TextStyle _style = TextStyle(fontSize: 18, fontWeight: FontWeight.bold);
    return Text(_text, style: _style,);
  }

  Widget _buildSubCardRepsText(
      BuildContext context, List<String> reps, int setIndex) {
    String _text = reps[setIndex] + ' reps';
    TextStyle _style = TextStyle(fontSize: 18, fontWeight: FontWeight.bold);
    return Text(_text, style: _style);
  }
*/
  /// Function _buildViewMore
  /// Description: Returns a formatted button to see all remaining routines (if ...
  /// ... there are more -- see _buildColumn).
  Widget _buildViewMore() {
    return RaisedButton(
      child: Text('View More'),
      onPressed: () {},
    );
  }
}
