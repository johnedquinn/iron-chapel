

import 'package:flutter/material.dart';
import 'package:iron_chapel/classes/history_info.dart';

import 'history_card_date.dart';

class HistoryCardWidget extends StatelessWidget {

  HistoryInfo historyInfo;
  int index;

  HistoryCardWidget({@required this.historyInfo, @required index});

  @override
  Widget build(BuildContext context) {
    return _buildHistoryCard(context);
  }

  Widget _buildHistoryCard(BuildContext context) {
    int index = this.historyInfo.getData()['completed'].length - 1;
    return Card(
        color: Color(4293848814),
        margin: EdgeInsets.fromLTRB(15.0, 7.0, 15.0, 7.0),
        elevation: 2,
        child: Container(
          height: 110,
          child: Row(
            children: <Widget>[
              _buildHistoryCardInfoSection(index),
              _buildHistoryCardRepsList(index)
            ],
          ),
        ));
  }

    Widget _buildHistoryCardInfoSection(int cardIndex) {
    DateTime _date = this.historyInfo.getData()['completed'][cardIndex]['date'].toDate();
    String _attemptedSetsAndReps = this.historyInfo.getData()['completed'][cardIndex]['attempted'];
    return Container(
      width: 100,
      padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
      child: Center(
          child: HistoryCardDate(_date, attemptedSetsAndReps: _attemptedSetsAndReps, isHistory: true)),
    );
  }

    Widget _buildHistoryCardRepsList(int cardIndex) {
      int _itemCount = this.historyInfo.getData()['completed'][cardIndex]['reps'].split('/').length;
    return Expanded(
        child: Container(
      padding: EdgeInsets.all(6),
      child: ListView.separated(
        scrollDirection: Axis.horizontal,
        itemCount: _itemCount,
        padding: new EdgeInsets.symmetric(vertical: 2.0),
        itemBuilder: (BuildContext context, int setIndex) {
          return _buildWeightsAndRepsSubCard(context, cardIndex, setIndex);
        },
        separatorBuilder: (BuildContext context, int index) {
          return Container(
            width: 5,
            padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
          );
        },
      ),
    ));
  }

    Widget _buildWeightsAndRepsSubCard(
      BuildContext context, int cardIndex, int setIndex) {
    return Container(
      height: 10,
      width: 100,
      child: Column(
        children: <Widget>[
          _buildSubCardSetTitle(context, setIndex),
          _buildSubCard(context, cardIndex, setIndex)
        ],
      ),
    );
  }

    Widget _buildSubCardSetTitle(BuildContext context, int index) {
    return Text('Set ' + (index + 1).toString());
  }

  Widget _buildSubCard(BuildContext context, int cardIndex, int setIndex) {
    List<String> _weights =
        this.historyInfo.getData()['completed'][cardIndex]['weights'].split('/');
    List<String> _reps =
        this.historyInfo.getData()['completed'][cardIndex]['reps'].split('/');
    return Expanded(
      child: Card(
        color: Colors.white,
        child: Container(
          constraints: BoxConstraints.expand(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _buildSubCardWeightText(context, _weights, setIndex),
              Divider(),
              _buildSubCardRepsText(context, _reps, setIndex)
            ],
          ),
        ),
      ),
    );
  }

    Widget _buildSubCardWeightText(
      BuildContext context, List<String> weights, int setIndex) {
    String _text = weights[setIndex] + ' lbs.';
        TextStyle _style = TextStyle(fontSize: 18, fontWeight: FontWeight.bold);
    return Text(_text, style: _style,);
  }

  Widget _buildSubCardRepsText(
      BuildContext context, List<String> reps, int setIndex) {
    String _text = reps[setIndex] + ' reps';
    TextStyle _style = TextStyle(fontSize: 18, fontWeight: FontWeight.bold);
    return Text(_text, style: _style);
  }
}