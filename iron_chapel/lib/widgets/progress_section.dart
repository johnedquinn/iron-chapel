/*
 * File: progress_section.dart
 * Author: John Ed Quinn
 * Description: Builds the container containing the progress title and the card ...
 * ... containing the chart title and progress chart.
*/

/* Imports */
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:iron_chapel/classes/exercise_strength_index.dart';
import 'package:iron_chapel/classes/history_info.dart';

/* Progress Section */
class ProgressSection extends StatelessWidget {

  /// Variables
  HistoryInfo historyInfo;  // HistoryInfo containing the history of a user's exercise.

  /// Function: ProgressSection
  /// Description: Constructor
  ProgressSection({@required this.historyInfo}) {
    print('PROGRESS SECTION');
    print(this.historyInfo);
  }

  /// Function: build
  /// Description: Main build method
  @override
  Widget build(BuildContext context) {
    return _buildProgressSection(context);
  }

  /// Function: _buildProgressSection
  /// Description: Returns the Column for the ProgressSection title ...
  /// ... and the ProgressCard.
  Widget _buildProgressSection(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[_getProgressTitle(), _getProgressCard()],
      ),
    );
  }

  /// Function: _getProgressTitle
  /// Description: Returns the title of the ProgressSection. Outside ...
  /// ... of the ProgressCard.
  Widget _getProgressTitle() {
    EdgeInsetsGeometry _padding = EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 10.0);
    TextStyle _style = TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold);
    return Container(child: Text('Progress', style: _style), padding: _padding);
  }

  /// Function: _getProgressCard
  /// Description: Returns the card containing the chart title and chart.
  Widget _getProgressCard() {
    EdgeInsetsGeometry _padding = EdgeInsets.fromLTRB(15, 0, 15, 0);
    return Card(
      margin: _padding,
      elevation: 4,
      child: Container(
        height: 300,
        child: _getProgressCardColumn(),
      ),
    );
  }

  /// Function: _getProgressCardColumn
  /// Description: Returns the column containing the progress chart title ...
  /// ... and the chart. Contained within the progress card.
  Widget _getProgressCardColumn() {
    return Column(
      children: <Widget>[
        _getProgressChartTitle(),
        _getProgressChart(),
      ],
    );
  }

  /// Function: _getProgressChartTitle
  /// Description: Returns the chart title contained withing the progress card.
  Widget _getProgressChartTitle() {
    EdgeInsetsGeometry _padding = EdgeInsets.fromLTRB(5.0, 12.0, 5.0, 5.0);
    TextStyle _style = TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold);
    return Container(
        child: Text(
          'ESI over Time',
          style: _style,
        ),
        padding: _padding);
  }

  /// Function: _getProgressChart
  /// Description: Returns the progress chart. Gets information using function '_getEsiData'.
  Widget _getProgressChart() {
    var _data = _getEsiData();
    List<charts.Series<ExerciseStrengthIndex, DateTime>> seriesList =
        List<charts.Series<ExerciseStrengthIndex, DateTime>>();
    seriesList.add(charts.Series(
      colorFn: (__, _) => charts.ColorUtil.fromDartColor(Colors.teal),
      id: 'ESI Indexes',
      data: _data,
      domainFn: (ExerciseStrengthIndex esi, _) => esi.date,
      measureFn: (ExerciseStrengthIndex esi, _) => esi.strength,
    ));
    Duration _time = Duration(milliseconds: 500);
    bool _animate = true;
    EdgeInsetsGeometry _padding = EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0);
    return new Container(
        height: 250,
        padding: _padding,
        child: charts.TimeSeriesChart(
          seriesList,
          dateTimeFactory: const charts.LocalDateTimeFactory(),
          defaultRenderer:
              charts.LineRendererConfig(includeArea: true, stacked: true),
          animate: _animate,
          animationDuration: _time,
        ));
  }

  /// Function: _getEsiData
  /// Description: Returns a list of ExerciseStrengthIndexes to be used by ...
  /// ... the progress chart.
  dynamic _getEsiData() {
    List<ExerciseStrengthIndex> list = List<ExerciseStrengthIndex>();
    int size = this.historyInfo.getData()['completed'].length;
    for (int i = 0; i < size; i++) {
      int strength = (i + 1) * 5;
      DateTime date =
          this.historyInfo.getData()['completed'][i]['date'].toDate();
      list.add(new ExerciseStrengthIndex(strength, date));
    }
    return list;
  }

}