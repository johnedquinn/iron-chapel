/*
 * File: history_card_date.dart
 * Author: John Ed Quinn
 * Description: The HistoryCardDate is used within the HistoryCardWidget. It ...
 * ... holds a column of the date information and the attempted sets/reps.
*/

/* Imports */
import 'package:flutter/material.dart';

/* Class: HistoryCardDate */
class HistoryCardDate extends StatelessWidget {

  /// Variables
  DateTime date;                // Date of when an exercise was performed
  String attemptedSetsAndReps;  // The '4x8' string of what was attempted
  bool isHistory;               // Determines if an exercise instance was in the past or present

  /// Function: HistoryCardDate
  /// Description: constructor
  HistoryCardDate(this.date, {@required this.attemptedSetsAndReps, @required this.isHistory});

  /// Function: build
  /// Description: Main build method.
  @override
  Widget build(BuildContext context) {
    return _buildDate();
  }

  /// Function: _buildDate
  /// Description: Creates the main column holding the text widgets and divider
  Widget _buildDate() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        _dayOfWeek(),
        _date(),
        _year(),
        Divider(
          height: 5.0,
        ),
        _attemptedTitle(),
        _attemptedSetsAndReps()
      ],
    );
  }

  /// Function: _dayOfWeek
  /// Description: Returns the widget containing the day of week ('MON')
  Widget _dayOfWeek() {
    TextStyle _style = TextStyle(fontWeight: FontWeight.bold);
    String _day = getDayOfWeek();
    return Text(
      _day,
      style: _style,
    );
  }

  /// Function: _date
  /// Description: Returns a text widget containing date and month.
  Widget _date() {
    String _date = getDate();
    TextStyle _style = TextStyle(fontWeight: FontWeight.bold);
    return Text(_date, style: _style);
  }

  /// Function: _year
  /// Description: Returns a text widget containing year
  Widget _year() {
    String _year = this.date.year.toString();
    TextStyle _style = TextStyle(fontWeight: FontWeight.bold);
    return Text(
      _year,
      style: _style,
    );
  }

  /// Function: _attemptedTitle
  /// Description: If this.isHistory, returns a text widget of 'Attempted', else is 'Attempting'
  Widget _attemptedTitle() {
    String _text = (this.isHistory) ? 'Attempted' : 'Attempting';
    TextStyle _style = TextStyle(fontWeight: FontWeight.bold);
    return Text(
      _text,
      style: _style,
    );
  }

  /// Function: _attemptedSetsAndReps
  /// Description: Returns the text widget containing attemptedSetsAndReps
  Widget _attemptedSetsAndReps() {
    TextStyle _style = TextStyle(fontWeight: FontWeight.bold);
    String _text = this.attemptedSetsAndReps;
    return Text(
      _text,
      style: _style,
    );
  }

  /// Function: getDayOfWeek
  /// Description: Returns a string in format 'MON', 'TUE', etc.
  String getDayOfWeek() {
    int day = this.date.weekday;
    switch (day) {
      case (1) : return 'MON';
      case (2) : return 'TUE';
      case (3) : return 'WED';
      case (4) : return 'THU';
      case (5) : return 'FRI';
      case (6) : return 'SAT';
      case (7) : return 'SUN';
    }
    return 'N/A';
  }

  /// Function: getDate
  /// Description: Returns a string in format '01 JAN'.
  String getDate() {
    String date = this.date.day.toString().padLeft(2, '0');
    int monthInt = this.date.month;
    String month;
    switch (monthInt) {
      case (1) : month = 'JAN'; break;
      case (2) : month = 'FEB'; break;
      case (3) : month = 'MAR'; break;
      case (4) : month = 'APR'; break;
      case (5) : month = 'MAY'; break;
      case (6) : month = 'JUN'; break;
      case (7) : month = 'JUL'; break;
      case (8) : month = 'AUG'; break;
      case (9) : month = 'SEP'; break;
      case (10) : month = 'OCT'; break;
      case (11) : month = 'NOV'; break;
      case (12) : month = 'DEC'; break;
    }
    return date + ' ' + month;
  }

}
