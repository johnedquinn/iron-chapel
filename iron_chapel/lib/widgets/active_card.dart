import 'package:flutter/material.dart';

import 'history_card_date.dart';

class ActiveCardWidget extends StatelessWidget {
  //HistoryInfo historyInfo;
  var exerciseInfo;

  List<int> weights;
  List<int> reps;

  ActiveCardWidget({@required this.exerciseInfo});
  //int cardIndex;

  @override
  Widget build(BuildContext context) {
    return _buildHistoryCard(context);
  }

  Widget _buildHistoryCard(BuildContext context) {
    //int index = this.historyInfo.getData()['completed'].length - 1;
    return Card(
        color: Color(4293848814),
        margin: EdgeInsets.fromLTRB(15.0, 7.0, 15.0, 7.0),
        elevation: 2,
        child: Container(
          height: 110,
          child: Row(
            children: <Widget>[
              _buildHistoryCardInfoSection(),
              _buildHistoryCardRepsList()
            ],
          ),
        ));
  }

  Widget _buildHistoryCardInfoSection() {
    // DateTime _date =
    //     this.historyInfo.getData()['completed'][cardIndex]['date'].toDate();
    // String _attemptedSetsAndReps =
    //     this.historyInfo.getData()['completed'][cardIndex]['attempted'];
    DateTime _date = DateTime.now();
    String _attemptedSetsAndReps = this.exerciseInfo['sets'].toString() + 'x' + this.exerciseInfo['reps'].toString();
    return Container(
      width: 100,
      padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
      child: Center(
          child: HistoryCardDate(_date,
              attemptedSetsAndReps: _attemptedSetsAndReps, isHistory: false,)),
    );
  }

  Widget _buildHistoryCardRepsList() {
    return Expanded(
        child: Container(
      padding: EdgeInsets.all(6),
      child: ListView.separated(
        scrollDirection: Axis.horizontal,
        itemCount: this.exerciseInfo['sets'],
        padding: new EdgeInsets.symmetric(vertical: 2.0),
        itemBuilder: (BuildContext context, int setIndex) {
          return _buildWeightsAndRepsSubCard(context, setIndex);
        },
        separatorBuilder: (BuildContext context, int index) {
          return Container(
            width: 5,
            padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
          );
        },
      ),
    ));
  }

  Widget _buildWeightsAndRepsSubCard(
      BuildContext context, int setIndex) {
    return Container(
      height: 10,
      width: 100,
      //color: Colors.white,
      child: Column(
        children: <Widget>[
          _buildSubCardSetTitle(context, setIndex),
          _buildSubCard(context, setIndex)
        ],
      ),
    );
  }

  Widget _buildSubCardSetTitle(BuildContext context, int index) {
    return Text('Set ' + (index + 1).toString());
  }

  Widget _buildSubCard(BuildContext context, int setIndex) {
    // List<String> _weights = this
    //     .historyInfo
    //     .getData()['completed'][cardIndex]['weights']
    //     .split('/');
    // List<String> _reps =
    //     this.historyInfo.getData()['completed'][cardIndex]['reps'].split('/');
    return Expanded(
      child: Card(
        color: Colors.white,
        child: Container(
          constraints: BoxConstraints.expand(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _buildSubCardWeightTextForm(context, setIndex),
              _buildSubCardRepsTextForm(context, setIndex)
            ],
          ),
        ),
      ),
    );
  }

  /*Widget _buildSubCardWeightText(
      BuildContext context, List<String> weights, int setIndex) {
    String _text = weights[setIndex] + ' lbs.';
        TextStyle _style = TextStyle(fontSize: 18, fontWeight: FontWeight.bold);
    return Text(_text, style: _style,);
  }*/

  Widget _buildSubCardWeightTextForm(BuildContext context, int setIndex) {
    TextStyle _style = TextStyle(fontSize: 18, fontWeight: FontWeight.bold);
    return TextFormField(
      style: _style,
      textAlign: TextAlign.center,
      decoration:
          InputDecoration(border: InputBorder.none, hintText: 'Weight (lbs)', contentPadding: EdgeInsets.fromLTRB(0, 2, 0, 4)),
      keyboardType: TextInputType.number,
    );
  }

  /*Widget _buildSubCardRepsText(
      BuildContext context, List<String> reps, int setIndex) {
    String _text = reps[setIndex] + ' reps';
    TextStyle _style = TextStyle(fontSize: 18, fontWeight: FontWeight.bold);
    return Text(_text, style: _style);
  }*/

  Widget _buildSubCardRepsTextForm(BuildContext context, int setIndex) {
    TextStyle _style = TextStyle(fontSize: 18, fontWeight: FontWeight.bold);
    return TextFormField(
      style: _style,
      textAlign: TextAlign.center,
      decoration:
          InputDecoration(border: InputBorder.none, hintText: 'Reps', contentPadding: EdgeInsets.fromLTRB(0, 4, 0, 2)),
      keyboardType: TextInputType.number,
    );
  }
}
