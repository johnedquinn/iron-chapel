import 'package:flutter/material.dart';
import 'package:iron_chapel/controllers/history_controller.dart';
import 'package:iron_chapel/classes/exercise_info.dart';
import 'package:iron_chapel/classes/history_info.dart';

class ExercisePage extends StatefulWidget {
  /// Private variables
  String _exerciseID;
  ExerciseInfo exerciseInfo = ExerciseInfo();
  HistoryInfo historyInfo = HistoryInfo();

  /// Constructor
  ExercisePage(this._exerciseID, {@required this.exerciseInfo, @required this.historyInfo}) {
    print('EXERCISE PAGE | Exercise ID = ' + this._exerciseID.toString());
    print('ExerciseInfo = ' + this.exerciseInfo.toString());
    print('HistoryInfo = ' + this.historyInfo.toString());
    print(this.exerciseInfo.getData()['sets']);
  }

  @override
  State<StatefulWidget> createState() {
    return _ExercisePageState();
  }
}

class _ExercisePageState extends State<ExercisePage> {
  //ExerciseInfo exerciseInfo = ExerciseInfo();
  //HistoryInfo historyInfo = HistoryInfo();

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: _buildPage(context));
  }

  Widget _buildPage(BuildContext context) {
    /*return CustomScrollView(
      slivers: <Widget>[_buildAppBar(context), _buildList(context)],
    );*/
    return FutureBuilder(
        future: _buildPageFuture(context),
        initialData: Center(child: CircularProgressIndicator()),
        builder: (BuildContext context, AsyncSnapshot<Widget> widg) {
          return widg.data;
        });
  }

  Future<Widget> _buildPageFuture(BuildContext context) async {
    if (widget.exerciseInfo.getData() == null)
      await widget.exerciseInfo.getExerciseDocument(widget._exerciseID);
    if (widget.historyInfo.getData() == null)
      await widget.historyInfo.getHistoryDocument(widget._exerciseID);
    return CustomScrollView(
      slivers: <Widget>[_buildAppBar(context), _buildList(context)],
    );
  }

  Widget _buildAppBar(BuildContext context) {
    return SliverAppBar(
      pinned: true,
      expandedHeight: 128.0,
      backgroundColor: Colors.grey,
      flexibleSpace: FlexibleSpaceBar(
        title: _getExerciseTitle(context, widget._exerciseID),
      ),
    );
  }

  Widget _buildList(BuildContext context) {
    return SliverList(
      delegate: SliverChildListDelegate([
        _getHistoryController(),
        _getExerciseDescriptionTitle(),
        _getExerciseDescription(),
        _getPmgTitle(),
        _getPmgs(),
        _getSmgTitle(),
        _getSmgs(),
        _getEndSpace()
      ]),
    );
  }

  /***** GET HISTORY CONTROLLER *****/
  Widget _getHistoryController() {
    return FutureBuilder(
        future: _getHistoryControllerWidget(),
        initialData: Center(child: CircularProgressIndicator()),
        builder: (BuildContext context, AsyncSnapshot<Widget> widg) {
          return widg.data;
        });
  }

  Future<Widget> _getHistoryControllerWidget() async {
    await widget.historyInfo.getHistoryDocument(widget._exerciseID);
    return HistoryController(widget.historyInfo, attemptedSets: widget.exerciseInfo.getData()['sets'], attemptedReps: widget.exerciseInfo.getData()['reps']);
  }

  Widget _getExerciseTitle(BuildContext context, String doc) {
    return FutureBuilder(
        future: _getExerciseTitleWidget(doc),
        initialData: Center(child: CircularProgressIndicator()),
        builder: (BuildContext context, AsyncSnapshot<Widget> widg) {
          return widg.data;
        });
  }

  Future<Widget> _getExerciseTitleWidget(String doc) async {
    await widget.exerciseInfo.getExerciseDocument(doc);
    return Text(widget.exerciseInfo.getData()['title']);
  }

  Widget _getExerciseDescriptionTitle() {
    EdgeInsetsGeometry _padding = EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 10.0);
    TextStyle _style = TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold);
    return Container(
        child: Text('Description', style: _style), padding: _padding);
  }

  Widget _getExerciseDescription() {
    return FutureBuilder(
        future: _getExerciseDescriptionWidget(),
        initialData: Center(child: CircularProgressIndicator()),
        builder: (BuildContext context, AsyncSnapshot<Widget> widg) {
          return widg.data;
        });
  }

  Future<Widget> _getExerciseDescriptionWidget() async {
    EdgeInsetsGeometry _padding = EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 10.0);
    await widget.exerciseInfo.getExerciseDocument(widget._exerciseID);
    return Container(
        child: Text(widget.exerciseInfo.getData()['description']),
        padding: _padding);
  }

  Widget _getPmgTitle() {
    EdgeInsetsGeometry _padding = EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 10.0);
    TextStyle _style = TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold);
    return Container(
        child: Text('Primary Muscle Groups', style: _style), padding: _padding);
  }

  Widget _getPmgs() {
    return FutureBuilder(
        future: _getPmgWidgets(),
        initialData: Center(child: CircularProgressIndicator()),
        builder: (BuildContext context, AsyncSnapshot<Widget> widg) {
          return widg.data;
        });
  }

  Future<Widget> _getPmgWidgets() async {
    await widget.exerciseInfo.getExerciseDocument(widget._exerciseID);
    EdgeInsetsGeometry _padding = EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0);
    return Container(
        height: 30,
        padding: _padding,
        child: ListView.separated(
          scrollDirection: Axis.horizontal,
          itemCount: widget.exerciseInfo.getData()['PMG'].length,
          itemBuilder: (BuildContext context, int index) {
            return mgWidget(widget.exerciseInfo.getData()['PMG'][index]);
          },
          separatorBuilder: (BuildContext context, int index) {
            return Container(
              width: 5,
            );
          },
        ));
  }

  Widget _getSmgTitle() {
    EdgeInsetsGeometry _padding = EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 10.0);
    TextStyle _style = TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold);
    return Container(
        child: Text('Secondary Muscle Groups', style: _style),
        padding: _padding);
  }

  Widget _getSmgs() {
    return FutureBuilder(
        future: _getSmgWidgets(),
        initialData: Center(child: CircularProgressIndicator()),
        builder: (BuildContext context, AsyncSnapshot<Widget> widg) {
          return widg.data;
        });
  }

  Future<Widget> _getSmgWidgets() async {
    await widget.exerciseInfo.getExerciseDocument(widget._exerciseID);
    EdgeInsetsGeometry _padding = EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0);
    return Container(
        height: 30,
        padding: _padding,
        child: ListView.separated(
          scrollDirection: Axis.horizontal,
          itemCount: widget.exerciseInfo.getData()['SMG'].length,
          itemBuilder: (BuildContext context, int index) {
            return mgWidget(widget.exerciseInfo.getData()['SMG'][index]);
          },
          separatorBuilder: (BuildContext context, int index) {
            return Container(
              width: 5,
            );
          },
        ));
  }

  Widget mgWidget(String text) {
    EdgeInsetsGeometry _padding = EdgeInsets.fromLTRB(12.0, 0.0, 12.0, 0.0);
    return Container(
      height: 30,
      padding: _padding,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: Colors.lightBlueAccent),
      child: Center(child: Text(text)),
    );
  }

  Widget _getEndSpace() {
    return Container(
      height: 50,
    );
  }
}

class Sales {
  int yearval;
  int salesval;
  Sales(this.yearval, this.salesval);
}
