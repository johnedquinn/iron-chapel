import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:iron_chapel/pages/workout.dart';

class RoutinePage extends StatefulWidget {
  String _routineString;

  RoutinePage(this._routineString) {
    print('\nROUTINE PAGE');
    print('Routine String: ' + this._routineString);
  }

  @override
  State<StatefulWidget> createState() {
    return _RoutinePageState();
  }
}

class _RoutinePageState extends State<RoutinePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: _buildPage(context));
  }

  Widget _buildPage(BuildContext context) {
    return StreamBuilder(
      stream: Firestore.instance
          .collection('/users/U3oI4eEr72P6ZX89O2XT2ls8sLx2/routines')
          .document(widget._routineString)
          .snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData)
          return Center(child: CircularProgressIndicator());
        return CustomScrollView(
          slivers: <Widget>[
            _buildAppBar(context, snapshot.data),
            _buildList(context, snapshot.data)
          ],
        );
      },
    );
  }

  Widget _buildAppBar(BuildContext context, DocumentSnapshot document) {
    return SliverAppBar(
      pinned: true,
      expandedHeight: 128.0,
      backgroundColor: Color(document['color']),
      flexibleSpace: FlexibleSpaceBar(
        title: Text(document['title']),
      ),
    );
  }

  Widget _buildList(BuildContext context, DocumentSnapshot document) {
    return SliverList(
      delegate: SliverChildListDelegate([
        _descriptionHeader(),
        _routineDescription(context, document),
        _workoutsHeader(),
        _buildWorkoutsColumn(context, document)
      ]),
    );
  }

  Widget _descriptionHeader() {
    return Container(
        padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 10.0),
        child: Text(
          'Routine Description',
          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
        ));
  }

  Widget _routineDescription(BuildContext context, DocumentSnapshot document) {
    return Container(
        padding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 10.0),
        child: Text(document['description']));
  }

  Widget _workoutsHeader() {
    return Container(
        padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 10.0),
        child: Text(
          'Workouts',
          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
        ));
  }

  Widget _buildWorkoutsColumn(BuildContext context, DocumentSnapshot document) {
    return Column(
      children: _buildWorkouts(document),
    );
  }

  List<Widget> _buildWorkouts(DocumentSnapshot document) {
    List<Widget> _columnItems = List<Widget>();
    for (int i = 0; i < document['workouts'].length; i++) {
      _columnItems.add(_buildListItem(context, document['workouts'][i]));
    }
    return _columnItems;
  }

  Widget _buildListItem(BuildContext context, var workout) {
    return Card(
      color: Color(4293848814),
      margin: EdgeInsets.fromLTRB(11.0, 7.0, 11.0, 7.0),
      child: ListTile(
        title: Text(workout['title']),
        subtitle: Text(workout['description'].substring(
            0,
            (workout['description'].length > 40)
                ? 40
                : workout['description'].length)),
        trailing: Icon(Icons.more_vert),
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => WorkoutPage(workout)));
        },
      ),
    );
  }
}
