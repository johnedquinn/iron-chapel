import 'package:flutter/material.dart';
import 'package:iron_chapel/classes/history_info.dart';
import 'package:iron_chapel/pages/active_workout.dart';

class ActiveWidget extends StatefulWidget {
  /// Private Variable
  var _workout;
  List<dynamic> exercises;
  List <HistoryInfo> histories;

  /// Constructor
  ActiveWidget(this._workout, this.exercises, {@required this.histories}) {
    print('\nACTIVE WIDGET');
    print('Workout: ' + this._workout.toString());
    print('Exercises: ' + this.exercises.toString());
  }

  /// Function: createState()
  /// Description: Create the state.
  @override
  _ActiveWidgetState createState() => _ActiveWidgetState();
}

class _ActiveWidgetState extends State<ActiveWidget> {
  /* Variables */
  OverlayEntry _overlayEntry;

  /// Function: build()
  /// Description: Builds the widget
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: Text('Begin Workout'),
      onPressed: () {
        this._overlayEntry = _createOverlayEntry();
        Overlay.of(context).insert(this._overlayEntry);
      },
    );
  }

  /// Function: _createOverlayEntry()
  /// Description: Creates the overlay for the active workout.
  OverlayEntry _createOverlayEntry() {
    return OverlayEntry(
      builder: (context) => ActiveWorkoutWidget(
          widget._workout,
          widget.exercises,
          MediaQuery.of(context).size.height,
          MediaQuery.of(context).size.width,
          finishWorkout,
          histories: widget.histories,),
    );
  }

  /// Function: finishWorkout
  /// Description: Function to be passed to the constructor...
  /// ... for the ActiveWorkoutWidget. Needed to changed the ...
  /// ... state of the overlay in this widget. It basically ...
  /// ... just removed the overlay from existence.
  void finishWorkout() {
    setState(() {
      this._overlayEntry.remove();
    });
  }
}
