import 'package:flutter/material.dart';
//import 'package:iron_chapel/subwidgets/nav_bar.dart';

class ProfilePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ProfilePageState();
  }
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: _buildProfilePage(context));
  }

  Widget _buildProfilePage(BuildContext context) {
    return CustomScrollView(
        slivers: <Widget>[_buildAppBar(context), _buildList(context)]);
  }

  Widget _buildAppBar(BuildContext context) {
    return SliverAppBar(
      pinned: true,
      expandedHeight: 128.0,
      backgroundColor: Colors.blue,
      flexibleSpace: FlexibleSpaceBar(
        title: Text('John Quinn'),
      ),
    );
  }

  Widget _buildList(BuildContext context) {
    return SliverList(delegate: SliverChildListDelegate([]));
  }
}
