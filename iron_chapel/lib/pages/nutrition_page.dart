import 'package:flutter/material.dart';

class NutritionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: _buildProfilePage(context));
  }

  Widget _buildProfilePage(BuildContext context) {
    return CustomScrollView(
        slivers: <Widget>[_buildAppBar(context), _buildList(context)]);
  }

  Widget _buildAppBar(BuildContext context) {
    return SliverAppBar(
      pinned: true,
      expandedHeight: 128.0,
      backgroundColor: Colors.blue,
      flexibleSpace: FlexibleSpaceBar(
        title: Text('Nutrition'),
      ),
    );
  }

  Widget _buildList(BuildContext context) {
    return SliverList(
       delegate: SliverChildListDelegate([
        _notAvailableYet()
        ],
       )
    );
  }

  Widget _notAvailableYet() {
    return Container(
      height: 600,
      child: Center(child: _notAvailableYetContent()),
    );
  }

  Widget _notAvailableYetContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        _foodIcon(),
        _notAvailableYetText(),
        _comingSoonText()
      ],
    );
  }

  Widget _foodIcon() {
    return Icon(Icons.local_dining, size: 100, color: Colors.grey,);
  }

  Widget _notAvailableYetText() {
    TextStyle _style = TextStyle(fontSize: 26, fontWeight: FontWeight.bold);
    return Text('Not Available Yet', style: _style);
  }

  Widget _comingSoonText() {
    TextStyle _style = TextStyle(fontSize: 12);
    return Text('Coming Soon', style: _style);
  }
}
