import 'package:flutter/material.dart';
import 'package:iron_chapel/classes/auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;
final GoogleSignIn _googleSignIn = GoogleSignIn();

class SignInPage extends StatefulWidget {

  const SignInPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _SignInPageState();
  }
}

class _SignInPageState extends State<SignInPage> {

    bool _success;
  String _userID;

  /* Build Method */
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            /* Top Space */
            Container(
              height: 30.0,
              width: 60.0,
            ),

            /* App Front Image */
            /*Image.asset(
              'assets/rand_logo.png',
              height: 180.0,
            ),*/

            /* Space between image and text */
            SizedBox(height: 20.0),

            /* App title */
            Text(
              'Iron Chapel',
              style: TextStyle(fontSize: 25.0, letterSpacing: 1.3, fontWeight: FontWeight.w100),
            ),

            /* Sized boxe between Text and Login Button */
            SizedBox(height: 50.0),

            /* LOGIN Button */
            FractionallySizedBox(
              widthFactor: 0.9,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: RaisedButton(
                      padding: EdgeInsets.all(20.0),
                      child: Text(
                        'Login',
                        style: TextStyle(fontSize: 17.0),
                      ),
                      onPressed: () {
                        Navigator.pushReplacementNamed(context, '/products');
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(9.0)),
                      color: Colors.teal,
                      textColor: Colors.white,
                    ),
                  ),
                ],
              ),
            ),

            /* Space between Login Button and Text */
            SizedBox(height: 50.0),

            /* Text directions */
            Text('Sign up with:'),

            /* Space between Text and Row of Buttons */
            SizedBox(height: 10.0),

            /* Row of Buttons */
            FractionallySizedBox(
              widthFactor: 0.9,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    flex: 15,
                    child: RaisedButton(
                      padding: EdgeInsets.all(20.0),
                      child: Text(
                        'Email',
                        style: TextStyle(fontSize: 17.0),
                      ),
                      onPressed: () {},
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(9.0)),
                      color: Colors.blueGrey,
                      textColor: Colors.white,
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(''),
                  ),
                  Expanded(
                    flex: 15,
                    child: RaisedButton(
                      padding: EdgeInsets.all(20.0),
                      child: Text(
                        'Google',
                        style: TextStyle(fontSize: 17.0),
                      ),
                      onPressed: () {_signInWithGoogle();},
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(9.0)),
                      color: Colors.red,
                      textColor: Colors.white,
                    ),
                  ),
                ],
              ),
            ),

          ],
        ),
      ),
    );
  } // End Build Method

    void _signInWithGoogle() async {
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;
    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    final FirebaseUser user =
        (await _auth.signInWithCredential(credential)).user;
    assert(user.email != null);
    assert(user.displayName != null);
    assert(!user.isAnonymous);
    assert(await user.getIdToken() != null);

    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);
    setState(() {
      if (user != null) {
        _success = true;
        _userID = user.uid;
      } else {
        _success = false;
      }
    });
  }

} // End Class _JQAuthPageState
