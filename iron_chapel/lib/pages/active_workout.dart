import 'package:flutter/material.dart';
import 'package:iron_chapel/classes/history_info.dart';
import 'package:iron_chapel/widgets/workout_timer.dart';
import 'package:iron_chapel/widgets/workout_carousel.dart';

class ActiveWorkoutWidget extends StatefulWidget {
  /* Variables */
  var _workout;
  List<dynamic> exercises;
  final double deviceHeight;
  final double deviceWidth;
  final Function finishWorkout;
  List <HistoryInfo> histories;

  /* Constructor */
  ActiveWorkoutWidget(this._workout, this.exercises, this.deviceHeight,
      this.deviceWidth, this.finishWorkout, {@required this.histories}) {
    print('\nACTIVE WORKOUT WIDGET');
    print('Workout: ' + this._workout.toString());
    print('Exercises: ' + this.exercises.toString());
  }

  /// Function: createState()
  /// Description: Create the state.
  @override
  _ActiveWorkoutWidgetState createState() => _ActiveWorkoutWidgetState();
}

class _ActiveWorkoutWidgetState extends State<ActiveWorkoutWidget> {
  /* Variables */
  Widget _content; // Variable content (either full-screen or small)
  double _height; // Height of screen
  double _width; // Width of screen
  double _bottom = 0.0; // Decides where the pop-up starts
  double _radius;
  double _left;

  /* Set Widgets to move to and back from */
  ListTile _stockListTile;
  Column _stockColumn;
  WorkoutTimer _workoutTimer = WorkoutTimer();

  /// Function: initState()
  /// Description: Initialize state
  @override
  void initState() {
    setState(() {
      _height = widget.deviceHeight - 50;
      _width = widget.deviceWidth;
      _radius = 30.0;
      _left = 0.0;
      _stockListTile = ListTile(
        leading: _workoutTimer,
        title: Text(
          'ListTile',
          style: TextStyle(color: Colors.white),
        ),
        subtitle:
            Text('Some Description', style: TextStyle(color: Colors.white)),
        trailing: Icon(
          Icons.arrow_upward,
          color: Colors.grey[50],
        ),
        onTap: () {
          switchWidget();
        },
      );
      _stockColumn = Column(
        children: <Widget>[
          GestureDetector(
            onVerticalDragDown: (DragDownDetails details) {
              switchWidget();
            },
            child: Icon(
              Icons.keyboard_arrow_down,
              color: Colors.white,
              size: 60.0,
            ),
          ),
          Container(
              width: _width,
              child: Text(
                'Summer 2019 Heavy | Chest Day',
                style: TextStyle(fontSize: 13.0, color: Colors.white),
              ),
              padding: EdgeInsets.fromLTRB(20.0, 28.0, 15.0, 0.0)),
          Container(
              width: _width,
              child: _workoutTimer,
              padding: EdgeInsets.fromLTRB(20.0, 0.0, 15.0, 10.0)),
          WorkoutCarousel(widget._workout, widget.exercises, histories: widget.histories,),
          RaisedButton(
            child: Text('Finish Workout'),
            onPressed: () {
              setState(() {
                _workoutTimer.stop();
                widget.finishWorkout();
              });
            },
          )
        ],
      );
      _content = _stockColumn;
    });
    super.initState();
  }

  /// Function: build()
  /// Description: Builds the widget
  @override
  Widget build(BuildContext context) {
    return Positioned(
        left: _left,
        bottom: _bottom,
        child: Material(
          elevation: 10.0,
          color: Color(0xFF1C2938),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(_radius),
                  topRight: Radius.circular(_radius))), //circular(_radius)),
          child: AnimatedContainer(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40.0),
                    topRight: Radius.circular(40.0))),
            duration: Duration(milliseconds: 300),
            child: SingleChildScrollView(child: _content, scrollDirection: Axis.vertical,),
            height: _height,
            width: this._width,
          ),
        ));
  }

  /// Function: switchWidget()
  /// Description: Switch the workout display from full-screen to a tab and vice-versa.
  void switchWidget() {
    setState(() {
      _height = (_height == widget.deviceHeight - 50)
          ? 75.0
          : widget.deviceHeight - 50;
      _width = (_width == widget.deviceWidth)
          ? widget.deviceWidth - 30
          : widget.deviceWidth;
      _bottom = (_bottom == 0.0) ? 75.0 : 0.0;
      _radius = (_radius == 30.0) ? 0.0 : 30.0;
      _left = (_left == 0.0) ? 15.0 : 0.0;
      if (_content is Column)
        _content = _stockListTile;
      else
        _content = _stockColumn;
    });
  }
}
