import 'package:flutter/material.dart';
import 'package:iron_chapel/classes/exercise_info.dart';
import 'package:iron_chapel/classes/history_info.dart';
import 'package:iron_chapel/pages/active.dart';
import 'package:iron_chapel/pages/exercise.dart';

class WorkoutPage extends StatefulWidget {
  var _workout;

  WorkoutPage(this._workout) {
    print('\nWORKOUT PAGE');
    print('Workout: ' + this._workout.toString());
  }

  @override
  State<StatefulWidget> createState() {
    return _WorkoutPageState();
  }
}

class _WorkoutPageState extends State<WorkoutPage> {
  //List<dynamic> exercises = new List<dynamic>();

  List<ExerciseInfo> exercises = List<ExerciseInfo>();
  List<HistoryInfo> histories = List<HistoryInfo>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: _buildPage(context));
  }

  Widget _buildPage(BuildContext context) {
    /*return CustomScrollView(
      slivers: <Widget>[
        _buildAppBar(context),
        _buildList(context),
      ],
    );*/
    return FutureBuilder(
        future: _buildPageFuture(context),
        initialData: Center(child: CircularProgressIndicator()),
        builder: (BuildContext context, AsyncSnapshot<Widget> widg) {
          return widg.data;
        });
  }

  Future<Widget> _buildPageFuture(BuildContext context) async {
    await _buildExercisesColumnWidget(context);
    return CustomScrollView(
      slivers: <Widget>[
        _buildAppBar(context),
        _buildList(context),
      ],
    );
  }

  Widget _buildAppBar(BuildContext context) {
    return SliverAppBar(
      pinned: true,
      expandedHeight: 128.0,
      flexibleSpace: FlexibleSpaceBar(
        title: Text(widget._workout['title']),
      ),
    );
  }

  Widget _buildList(BuildContext context) {
    return FutureBuilder(
        future: _buildListFuture(context),
        initialData: SliverList(delegate: SliverChildListDelegate([Center(child: CircularProgressIndicator())]),),
        builder: (BuildContext context, AsyncSnapshot<Widget> widg) {
          return widg.data;
        });
    /*return SliverList(
      delegate: SliverChildListDelegate([
        _beginWorkoutButton(context),
        _descriptionHeader(),
        _workoutDescription(context),
        _exercisesHeader(),
        _buildExercisesColumn(context)
      ]),
    );*/
  }

  Future<Widget> _buildListFuture(BuildContext context) async {
    await _buildExercisesColumnWidget(context);
    return SliverList(
      delegate: SliverChildListDelegate([
        _beginWorkoutButton(context),
        _descriptionHeader(),
        _workoutDescription(context),
        _exercisesHeader(),
        _buildExercisesColumn(context)
      ]),
    );
  }

  Widget _beginWorkoutButton(BuildContext context) {
    return Container(
        padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        child: ActiveWidget(
          widget._workout,
          this.exercises,
          histories: this.histories,
        ));
  }

  Widget _descriptionHeader() {
    return Container(
        padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 10.0),
        child: Text(
          'Workout Description',
          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
        ));
  }

  Widget _workoutDescription(BuildContext context) {
    return Container(
        padding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 10.0),
        child: Text(widget._workout['description']));
  }

  Widget _exercisesHeader() {
    return Container(
        padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 10.0),
        child: Text(
          'Exercises',
          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
        ));
  }

  Widget _buildExercisesColumn(BuildContext context) {
    print('BUILD EXERCISE COLUMN');
    /*return FutureBuilder(
        future: _buildExercisesColumnWidget(context),
        initialData: Center(child: CircularProgressIndicator()),
        builder: (BuildContext context, AsyncSnapshot<Widget> widg) {
          return widg.data;
        });*/
    return Column(
      children: _buildExercises(),
    );
  }

  Future<Widget> _buildExercisesColumnWidget(BuildContext context) async {
    for (int i = 0; i < widget._workout['exercises'].length; i++) {
      if (this.exercises.length <= i) {
        exercises.insert(i, ExerciseInfo());
        histories.insert(i, HistoryInfo());
      }
      await exercises[i].getExerciseDocument(
          widget._workout['exercises'][i]['path'].documentID);
      await histories[i].getHistoryDocument(
          widget._workout['exercises'][i]['path'].documentID);
    }
    return Column(
      children: _buildExercises(),
    );
  }

  List<Widget> _buildExercises() {
    List<Widget> _columnItems = List<Widget>();
    for (int i = 0; i < widget._workout['exercises'].length; i++) {
      _columnItems.add(_buildListItem(context, i));
    }
    return _columnItems;
  }

  Widget _buildListItem(BuildContext context, int index) {
    return Card(
        color: Color(4293848814),
        margin: EdgeInsets.fromLTRB(11.0, 7.0, 11.0, 7.0),
        child: buildFutureCarouselExercise(index));
  }

  /// Builds the future exercise card
  Widget buildFutureCarouselExercise(int index) {
    return ListTile(
      title: Text(this.exercises[index].getData()['title']),
      subtitle: Text(widget._workout['exercises'][index]['sets'].toString() +
          'x' +
          widget._workout['exercises'][index]['reps'].toString()),
      trailing: Icon(Icons.more_vert),
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => ExercisePage(
                    widget._workout['exercises'][index]['path'].documentID,
                    exerciseInfo: this.exercises[index],
                    historyInfo: this.histories[index])));
      },
    );
  }
}
