import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
//import 'dart:math';
//import 'package:iron_chapel/classes/fitness.dart';

class RoutinesPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _RoutinesPageState();
  }
}

class _RoutinesPageState extends State<RoutinesPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildContent(context),
    );
  }

  /// Function: _buildContent
  /// Description: This builds the customized AppBar and the customized ListView ...
  /// ... that contains all information.
  _buildContent(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          pinned: true,
          expandedHeight: 128.0,
          flexibleSpace: FlexibleSpaceBar(
            title: Text('Workout'),
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate([
            _yourRoutinesHeader(),
            _buildStream(context,
                '/users/U3oI4eEr72P6ZX89O2XT2ls8sLx2/routines'), // TODO : Get rid of hard-coded path.
            _popularRoutinesHeader(),
            _buildStream(context, '/routines')
          ]),
        )
      ],
    );
  }

  /// Function: _buildStream
  /// Description: This function goes into the Firestore with the target path. It ...
  /// ... retrieves all documents contained at the end of the path and returnes a ...
  /// ... column of these documents (these are passed to _buildColumn)
  Widget _buildStream(BuildContext context, String target) {
    return StreamBuilder(
        stream: Firestore.instance.collection(target).snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return Center(child: CircularProgressIndicator());
          return Column(
            children: _buildColumn(context, snapshot.data.documents),
          );
        });
  }

  /// Function: _buildColumn
  /// Description: Creates a list of list tiles (containing the routines retrieved ...
  /// ... from the stream).
  List<Widget> _buildColumn(BuildContext context, var documents) {
    int maxLength = 3;
    bool isLong = documents.length > maxLength;
    int size = (isLong) ? maxLength : documents.length;
    List<Widget> _columnItems = List<Widget>();
    for (int i = 0; i < size; i++) {
      _columnItems.add(_buildListItem(context, documents[i]));
    }
    if (isLong) _columnItems.add(_buildViewMore());
    return _columnItems;
  }

  /// Function: _buildListItem
  /// Description: Creates a list item with a specific routine's information.
  Widget _buildListItem(BuildContext context, DocumentSnapshot document) {
    return Card(
      color: Color(document['color']),
      margin: EdgeInsets.fromLTRB(11.0, 7.0, 11.0, 7.0),
      child: ListTile(
        title: Text(document['title']),
        subtitle: Text(document['description'].substring(
                0,
                (document['description'].length > 40)
                    ? 40
                    : document['description'].length) +
            '...'),
        trailing: Icon(Icons.more_vert),
        onTap: () {
          Navigator.pushNamed(context, '/routine/' + document.documentID);
        },
      ),
    );
  }

  /// Function: _yourRoutinesHeader
  /// Description: Returns formatted text ("Your Routines")
  Widget _yourRoutinesHeader() {
    return Container(
        child: Text(
          'Your Routines',
          style: TextStyle(fontSize: 40.0, fontWeight: FontWeight.bold),
        ),
        padding: EdgeInsets.fromLTRB(20.0, 28.0, 20.0, 10.0));
  }

  /// Function: _popularRoutinesHeader
  /// Description: Returns formatted text ("Popular Routines")
  Widget _popularRoutinesHeader() {
    return Container(
        child: Text(
          'Popular Routines',
          style: TextStyle(fontSize: 40.0, fontWeight: FontWeight.bold),
        ),
        padding: EdgeInsets.fromLTRB(20.0, 28.0, 15.0, 10.0));
  }

  /// Function _buildViewMore
  /// Description: Returns a formatted button to see all remaining routines (if ...
  /// ... there are more -- see _buildColumn).
  Widget _buildViewMore() {
    return RaisedButton(
      child: Text('View More'),
      onPressed: () {},
    );
  }
}
